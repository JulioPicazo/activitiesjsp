package com.softtek.academy.javaweb.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softtek.academy.javaweb.model.ActivityBean;
import com.softtek.academy.javaweb.service.ActivityService;

/**
 * Servlet implementation class UpdateActivityController
 */
public class UpdateActivityController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateActivityController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		
		 ActivityService actServ = new ActivityService();
		 
	        Boolean validate = actServ.updActs(Integer.parseInt(request.getParameter("id")),
	        Integer.parseInt(request.getParameter("done")));
	        
	        if(validate)
	        {
	            List<ActivityBean> actLista = actServ.getActs(0 ,true);
	            RequestDispatcher rd = request.getRequestDispatcher("/views/tableActivities.jsp");
	            request.setAttribute("list", actLista);
	            rd.forward(request, response);
	        }
	        else 
	        {
	            RequestDispatcher rd = request.getRequestDispatcher("/views/error.jsp");
	            rd.forward(request, response);
	        }

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
