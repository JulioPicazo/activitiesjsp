package com.softtek.academy.javaweb.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softtek.academy.javaweb.model.ActivityBean;
import com.softtek.academy.javaweb.service.ActivityService;

/**
 * Servlet implementation class GetActivitiesController
 */
public class GetActivitiesController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetActivitiesController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		
		 List<ActivityBean> arrey = new ArrayList<ActivityBean>();
	        ActivityService actServ = new ActivityService();
	        
	        String askDone = request.getParameter("done");
	        
	        if (askDone != null && !askDone.isEmpty()) 
	        { 
	        	arrey = actServ.getActs(Integer.parseInt(askDone), false);
	        } 
	        else 
	        {
	        	arrey = actServ.getActs(0, true);
	        }
	        RequestDispatcher rd = request.getRequestDispatcher("/views/tableActivities.jsp");
	        request.setAttribute("list", arrey);
	        rd.forward(request, response);

	}
}
