package com.softtek.academy.javaweb.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softtek.academy.javaweb.model.ActivityBean;
import com.softtek.academy.javaweb.service.ActivityService;

/**
 * Servlet implementation class ControllerActivity
 */
public class ControllerActivity extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ControllerActivity() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		
		String act = request.getParameter("list");
        Boolean is_done = Boolean.valueOf(request.getParameter("done"));
        
        ActivityService actServ = new ActivityService();
        
        int validate = actServ.AddActs(act, is_done  == false ? 0 : 1);
        
        ActivityService todoListService = new ActivityService();
        
        if(validate == 0)
        {
            List<ActivityBean> listaAct = todoListService.getActs(0 ,true);
            RequestDispatcher rd = request.getRequestDispatcher("/views/tableActivities.jsp");
            request.setAttribute("list", listaAct);
            rd.forward(request, response);
        } 
        else 
        {
            RequestDispatcher rd = request.getRequestDispatcher("/views/error.jsp");
            rd.forward(request, response);
        }	
	}
}
