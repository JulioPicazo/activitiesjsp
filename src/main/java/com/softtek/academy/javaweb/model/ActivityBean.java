package com.softtek.academy.javaweb.model;

public class ActivityBean {
	
	private int id;
	private String list;
	private int is_done;
	
	public ActivityBean(int id, String list, int is_done) {
		super();
		this.id = id;
		this.list = list;
		this.is_done = is_done;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getList() {
		return list;
	}
	public void setList(String list) {
		this.list = list;
	}
	public int getIs_done() {
		return is_done;
	}
	public void setIs_done(int is_done) {
		this.is_done = is_done;
	}
}
