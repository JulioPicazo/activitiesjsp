package com.softtek.academy.javaweb.model;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public final class Connections {

	private static Connections con;
	private Connection connection;
	private String url = "jdbc:mysql://localhost:3306/prueba?serverTimezone=UTC#";
	private String username = "root";
	private String password = "1234";

	private Connections() throws SQLException {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			this.connection = DriverManager.getConnection(url, username, password);
		} catch (ClassNotFoundException ex) {
			System.out.println("Database Connection Creation Failed : " + ex.getMessage());
		}
	}

	public Connection getConnection() {
		return connection;
	}

	public static Connections getInstance() throws SQLException {
		if (con == null) 
		{
			con = new Connections();
		} 
		else 
		{
			return con;
		}
		return con;

	}

}
