package com.softtek.academy.javaweb.service;

import java.util.List;

import com.softtek.academy.javaweb.DAO.AddActivityDAO;
import com.softtek.academy.javaweb.DAO.GetActivityDAO;
import com.softtek.academy.javaweb.DAO.UpdateActivityDAO;
import com.softtek.academy.javaweb.model.ActivityBean;

public class ActivityService {
	
	 public List<ActivityBean> getActs(int done, Boolean all){
	        GetActivityDAO GT = new GetActivityDAO();
	        return GT.getActs(done, all);
	    }

	    public Boolean updActs(int id, int done){
	        UpdateActivityDAO UPTDao = new UpdateActivityDAO();
	        return UPTDao.updAct(id, done);
	    }
	    
	    public int AddActs(String list, int done){
	    	AddActivityDAO CTD = new AddActivityDAO();
	        return CTD.addActivity(list,done);
	    }
}
