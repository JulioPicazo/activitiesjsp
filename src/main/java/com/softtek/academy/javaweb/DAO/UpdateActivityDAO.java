package com.softtek.academy.javaweb.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.softtek.academy.javaweb.model.ConexDB;

public class UpdateActivityDAO {
	
	public Boolean updAct(int id, int done) {
		
        try {
        	Connection con = ConexDB.getInstance().getConnection();
            PreparedStatement ps = con.prepareStatement("update TO_DO_LIST set is_done = ? where id = ?");
            ps.setInt(1, done);
            ps.setInt(2, id);
            int a = ps.executeUpdate();
            return a > 0 ? true : false;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

}
