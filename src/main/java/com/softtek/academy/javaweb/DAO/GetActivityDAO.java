package com.softtek.academy.javaweb.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.softtek.academy.javaweb.model.ActivityBean;
import com.softtek.academy.javaweb.model.ConexDB;

public class GetActivityDAO {
	
	public List<ActivityBean> getActs(int done, Boolean s) {
		
        List<ActivityBean> actVO = new ArrayList<ActivityBean>();
        
        try  {
        	Connection con = ConexDB.getInstance().getConnection();
        	ResultSet rs = null;
            if (s) 
            {
                rs = con.prepareStatement("select * from TO_DO_LIST").executeQuery();
            } 
            else 
            {
                PreparedStatement ps = con.prepareStatement("select * from TO_DO_LIST where is_done = ?");
                ps.setInt(1, done);
                rs = ps.executeQuery();
            }
            while (rs.next()) {
            	actVO.add(new ActivityBean(rs.getInt(1), rs.getString(2), rs.getInt(3)));
            }
            return actVO;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

}
