package com.softtek.academy.javaweb.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import com.softtek.academy.javaweb.model.ConexDB;

public class AddActivityDAO {
	
	public int addActivity(String act, int done) {
        try{
        	Connection con = ConexDB.getInstance().getConnection();
        	PreparedStatement ps = con.prepareStatement("insert into TO_DO_LIST (list, is_done) values (?, ?)",Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, act);
            ps.setInt(2, done);
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) 
            {
                return  rs.getInt(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
        return 0;
    }
}
