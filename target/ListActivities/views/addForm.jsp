<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
    <title>Add to do</title>
</head>
<body>
    <form action="ControllerActivity" method="post" id="form">
        <label for="list"> New Task</label>
        <input type="text" name="list">
        <label for="done"> Select done or do not done</label>
        <input type="checkbox" name="done">
        <input type="submit" value="Create Task">
    </form>
</body>
</html>
